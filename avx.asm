; AVX support detection and helpers.

[global avxSupported]

avxSupported:
    mov RAX, 1
    cpuid

    xor RAX, RAX
    bt RCX, 28
    adc RAX, RAX

    ret

