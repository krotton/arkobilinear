; Bilinear interpolation scaling

; uchar *getPoint(int x, int y)
; Stores the result in the specified register
%macro getPoint 2
    mov EAX, %2
    mov EDX, %1

    cmp EAX, [EBP+40]
    jae %%fixX
%%checkY:
    cmp EDX, [EBP+36]
    jae %%fixY

%%calc:
    mov EDI, EAX
    push EDX
    imul EDI, [EBP+36]
    pop EDX
    add EDI, EDX
    shl EDI, 2
    add EDI, [EBP+32]
    jmp %%end

%%fixX:
    sub EAX, 1
    jmp %%checkY

%%fixY:
    sub EDX, 1
    jmp %%end

%%end:
%endmacro

; void addPart(int argb, float *tmpX)
; Multiplies the value of the given channel at the given point by its weight
; and increases the temporary value.
%macro addPart 2
    movzx AX, byte [EDI+%1]     ; Zero extend the byte
    mov word [EBP-46], AX       ; Push the channel's value to the FPU
    fild word [EBP-46]
    fmul ST0, ST1               ; ST0 = w * v
    fadd dword %2               ; tempResult:X += w * v
    fstp dword %2               ; Pop from FPU!
%endmacro

; void scale(uchar *out, int X, int Y, const uchar *in, int x, int y)
[global scale]

scale:
    ; ESP+4 = out
    ; ESP+8 = outW
    ; ESP+12 = outH
    ; ESP+16 = in
    ; ESP+20 = inW
    ; ESP+24 = inH

    ; Save the callee-saved registers: EBX, ESI, EDI
    push EBX
    push ESI
    push EDI

    ; Initialize the stack frame
    push EBP
    mov EBP, ESP

    ; EBP+20 = out
    ; EBP+24 = outW
    ; EBP+28 = outH
    ; EBP+32 = in
    ; EBP+36 = inW
    ; EBP+40 = inH

    ; Allocate space for local data.
    sub ESP, 52

    ; Clear the FPU - it's ours now! ;).
    finit
    
    ; Calculate xRatio and yRatio: xRatio = inW/outW
    fild dword [EBP+36]         ; Push inW to FPU
    fidiv dword [EBP+24]        ; ST0 = inW/outW
    fst dword [EBP-4]           ; EBP-4 = xRatio = inW/outW !!

    fild dword [EBP+40]         ; Push inH to FPU
    fidiv dword [EBP+28]        ; ST0 = inH/outH
    fst dword [EBP-8]           ; EBP-8 = yRatio = inH/outH !!

    ; Now loop over every pixel of the output
    ; (we loop backwards to use zero-comparison).
    mov EBX, [EBP+28]           ; EBX = outH - 1
    dec EBX
    mov ECX, [EBP+24]           ; ECX = outW - 1
    dec ECX

loop:
    ; Calculate xSrc = xRatio * x:
    mov dword [EBP-12], ECX     ; Push x to FPU
    fild dword [EBP-12]
    fmul ST0, ST2   		; st0 = xRatio * x
    fld ST0                     ; Copy the xSrc, because fisttp will pop st0
    fisttp dword [EBP-24]	; EBP-24 = xInt = int(st0) !!
    fisub dword [EBP-24]	; st0 = xSrc - xInt
    fst dword [EBP-28]		; EBP-28 = wX = st0 !!
    
    ; Now the same with ySrc:
    mov dword [EBP-12], EBX     ; Push y to FPU
    fild dword [EBP-12]
    fmul ST0, ST2               ; st0 = yRatio * y
    fld ST0
    fisttp dword [EBP-16]       ; EBP-16 = yInt = int(st0) !!
    fisub dword [EBP-16]        ; st0 = ySrc - yInt
    fst dword [EBP-20]          ; EBP-20 = wY = st0 !!

    ; Calculate the addresses of adjacent points' data.
    ; For every point, calculate it's part of the weighted average
    ; and add it to the point's value.
    ; Repeat the calculation for every channel: A, R, G and B.
    ; Finally store the result in the output table.

    ; R15 = outPos = out + 4 * y * outW + 4 * x = out + 4 * (y * outW + x)
    mov ESI, EBX                ; ESI = y
    imul ESI, [EBP+24]          ; ESI *= outW
    add ESI, ECX                ; ESI += x
    shl ESI, 2                  ; ESI *= 4
    add ESI, [EBP+20]           ; ESI += out

    ; Put zeros in the memory used for storing temporary values
    mov dword [EBP-32], 0       ; tempRes:B = 0.0
    mov dword [EBP-36], 0       ; tempRes:G = 0.0
    mov dword [EBP-40], 0       ; tempRes:R = 0.0
    mov dword [EBP-44], 0       ; tempRes:A = 0.0

    ; Point (0, 0)
    getPoint dword [EBP-24], dword [EBP-16] ; EDI = (x + 0, y + 0)
    
    ; ST0 = Weight (0, 0)
    fld1                        ; ST0 = 1
    fsub ST0, ST2               ; ST0 = 1 - wX
    fld1                        ; ST0 = 1
    fsub ST0, ST2               ; ST0 = 1 - wY
    fmul ST0, ST1               ; ST0 = (1 - wY)*(1 - wX) = w !!

    ; Add parts from this point to the resulting values
    addPart 0, [EBP-32]         ; B
    addPart 1, [EBP-36]         ; G
    addPart 2, [EBP-40]         ; R
    addPart 3, [EBP-44]         ; A

    ; Clear data from FPU stack
    ffree st0                   ; w
    fincstp

    ; Point (0, 1)
    inc dword [EBP-16]
    getPoint dword [EBP-24], dword [EBP-16] ; EDI = (x + 0, y + 1)
    
    ; ST0 = Weight (0, 1)
    fmul ST0, ST1               ; ST0 = (1 - wX)*wY = w !!

    ; Add parts from this point to the resulting values
    addPart 0, [EBP-32]         ; B
    addPart 1, [EBP-36]         ; G
    addPart 2, [EBP-40]         ; R
    addPart 3, [EBP-44]         ; A

    ; Clear data from FPU stack
    ffree st0                   ; w
    fincstp

    ; Point (1, 1)
    inc dword [EBP-24]
    getPoint dword [EBP-24], dword [EBP-16] ; EDI = (x + 1, y + 1)
    
    ; ST0 = Weight (1, 1)
    fld ST0                     ; Copy wY
    fmul ST0, ST2               ; ST0 = wX*wY = w !!

    ; Add parts from this point to the resulting values
    addPart 0, [EBP-32]         ; B
    addPart 1, [EBP-36]         ; G
    addPart 2, [EBP-40]         ; R
    addPart 3, [EBP-44]         ; A

    ; Clear data from FPU stack
    ffree st0                   ; w
    fincstp

    ; Point (1, 0)
    dec dword [EBP-16]
    getPoint dword [EBP-24], dword [EBP-16] ; EDI = (x + 1, y + 0)
    
    ; ST0 = Weight (1, 0)
    fld1                        ; ST0 = 1
    fsub ST0, ST1               ; ST0 = 1 - wY
    fmul ST0, ST2               ; ST0 = wX*(1 - wY) = w !!

    ; Add parts from this point to the resulting values
    addPart 0, [EBP-32]         ; B
    addPart 1, [EBP-36]         ; G
    addPart 2, [EBP-40]         ; R
    addPart 3, [EBP-44]         ; A

    ; Clear data from FPU stack
    ffree st0                   ; w
    ffree st1                   ; wY
    ffree st2                   ; wX
    fincstp
    fincstp
    fincstp

    ; Truncate the pixel values and save them
    cvttss2si EAX, dword [EBP-32] ; B = (uchar)((int)tempResult:A)
    mov byte [ESI], AL          ; (*output:A) = A
    cvttss2si EAX, dword [EBP-36] ; G
    mov byte [ESI+1], AL
    cvttss2si EAX, dword [EBP-40] ; R
    mov byte [ESI+2], AL
    cvttss2si EAX, dword [EBP-44] ; A
    mov byte [ESI+3], AL

    ; Decrement the x pointer
    dec ECX
    jns loop

    ; Restore the previous x value
    mov ECX, [EBP+24]
    dec ECX

    ; Decrement the y pointer
    dec EBX
    jns loop

end: 
    ; Clear the stack frame
    mov ESP, EBP
    pop EBP

    ; Restore the callee-saved registers
    pop EDI
    pop ESI
    pop EBX

    ret

