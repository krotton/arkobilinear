; Bilinear interpolation scaling

; uchar *getPoint(int x, int y)
; Stores the result in the specified register
%macro getPoint 2
    mov R13D, %2
    mov R14D, %1

    cmp R13D, R9D
    jae %%fixX
%%checkY:
    cmp R14D, R8D
    jae %%fixY

%%calc:
    mov R12, R13
    imul R12D, R8D
    add R12D, R14D
    shl R12D, 2
    add R12, RCX
    jmp %%end

%%fixX:
    sub R13D, 1
    jmp %%checkY

%%fixY:
    sub R14D, 1
    jmp %%end

%%end:
%endmacro

; void addPart(int argb, float *tmpX)
; Multiplies the value of the given channel at the given point by its weight
; and increases the temporary value.
%macro addPart 2
    movzx R13W, byte [R12+%1]   ; Zero extend the byte
    mov word [RBP-46], R13W     ; Push the channel's value to the FPU
    fild word [RBP-46]
    fmul ST0, ST1               ; ST0 = w * v
    fadd dword %2               ; tempResult:X += w * v
    fstp dword %2               ; Pop from FPU!
%endmacro

; void scale(uchar *out, int X, int Y, const uchar *in, int x, int y)
[global scale]

scale:
    ; RDI = out
    ; RSI = outW
    ; RDX = outH
    ; RCX = in
    ; R8 = inW
    ; R9 = inH

    ; Save R12 - R15 and RBX
    push R12
    push R13
    push R14
    push R15
    push RBX

    ; Initialize the stack frame
    push RBP
    mov RBP, RSP

    ; Allocate 9 floats, three ints and a short.
    sub RSP, 52

    ; RDX is volatile, so store outH in memory.
    mov dword [RBP-52], EDX

    ; Clear the FPU - it's ours now! ;).
    finit
    
    ; Calculate xRatio and yRatio: xRatio = inW/outW
    mov [RBP-4], R8D            ; RBP-4 = inW
    mov [RBP-8], ESI            ; RBP-8 = outW
    fild dword [RBP-4]          ; Push inW to FPU
    fidiv dword [RBP-8]         ; ST0 = inW/outW
    fst dword [RBP-4]           ; RBP-4 = xRatio = inW/outW !!

    mov dword [RBP-8], R9D      ; RBP-8 = inH
    fild dword [RBP-8]          ; Push inH to FPU
    fidiv dword [RBP-52]        ; ST0 = inH/outH
    fst dword [RBP-8]           ; RBP-8 = yRatio = inH/outH !!

    ; Now loop over every pixel of the output
    ; (we loop backwards to use zero-comparison).
    mov R10, RDX                ; R10 = outH - 1
    dec R10
    mov R11, RSI                ; R11 = outW - 1
    dec R11

loop:
    ; Calculate xSrc = xRatio * x:
    mov dword [RBP-12], R11D    ; Push x to FPU
    fild dword [RBP-12]
    fmul ST0, ST2   		    ; st0 = xRatio * x
    fld ST0                     ; Copy the xSrc, because fisttp will pop st0
    fisttp dword [RBP-24]		; RBP-24 = xInt = int(st0) !!
    fisub dword [RBP-24]		; st0 = xSrc - xInt
    fst dword [RBP-28]			; RBP-28 = wX = st0 !!
    
    ; Now the same with ySrc:
    mov dword [RBP-12], R10D    ; Push y to FPU
    fild dword [RBP-12]
    fmul ST0, ST2               ; st0 = yRatio * y
    fld ST0
    fisttp dword [RBP-16]       ; RBP-16 = yInt = int(st0) !!
    fisub dword [RBP-16]        ; st0 = ySrc - yInt
    fst dword [RBP-20]          ; RBP-20 = wY = st0 !!

    ; Calculate the addresses of adjacent points' data.
    ; For every point, calculate it's part of the weighted average
    ; and add it to the point's value.
    ; Repeat the calculation for every channel: A, R, G and B.
    ; Finally store the result in the output table.

    ; R15 = outPos = out + 4 * y * outW + 4 * x = out + 4 * (y * outW + x)
    mov R15, R10                ; R15 = y
    imul R15D, ESI              ; R15 *= outW
    add R15D, R11D              ; R15 += x
    shl R15D, 2                 ; R15 *= 4
    add R15, RDI                ; R15 += out
                                ; R15 = outPos !!

    ; Put zeros in the memory used for storing temporary values
    mov qword [RBP-36], 0       ; tempRes:A = tempRes:R = 0.0
    mov qword [RBP-44], 0       ; tempRes:G = tempRes:B = 0.0

    ; Point (0, 0)
    getPoint dword [RBP-24], dword [RBP-16] ; R12 = (x + 0, y + 0)
    
    ; ST0 = Weight (0, 0)
    fld1                        ; ST0 = 1
    fsub ST0, ST2               ; ST0 = 1 - wX
    fld1                        ; ST0 = 1
    fsub ST0, ST2               ; ST0 = 1 - wY
    fmul ST0, ST1               ; ST0 = (1 - wY)*(1 - wX) = w !!

    ; Add parts from this point to the resulting values
    addPart 0, [RBP-32]         ; B
    addPart 1, [RBP-36]         ; G
    addPart 2, [RBP-40]         ; R
    addPart 3, [RBP-44]         ; A

    ; Clear data from FPU stack
    ffree st0                   ; w
    fincstp

    ; Point (0, 1)
    inc dword [RBP-16]
    getPoint dword [RBP-24], dword [RBP-16] ; R12 = (x + 0, y + 1)
    
    ; ST0 = Weight (0, 1)
    fmul ST0, ST1               ; ST0 = (1 - wX)*wY = w !!

    ; Add parts from this point to the resulting values
    addPart 0, [RBP-32]         ; B
    addPart 1, [RBP-36]         ; G
    addPart 2, [RBP-40]         ; R
    addPart 3, [RBP-44]         ; A

    ; Clear data from FPU stack
    ffree st0                   ; w
    fincstp

    ; Point (1, 1)
    inc dword [RBP-24]
    getPoint dword [RBP-24], dword [RBP-16] ; R12 = (x + 1, y + 1)
    
    ; ST0 = Weight (1, 1)
    fld ST0                     ; Copy wY
    fmul ST0, ST2               ; ST0 = wX*wY = w !!

    ; Add parts from this point to the resulting values
    addPart 0, [RBP-32]         ; B
    addPart 1, [RBP-36]         ; G
    addPart 2, [RBP-40]         ; R
    addPart 3, [RBP-44]         ; A

    ; Clear data from FPU stack
    ffree st0                   ; w
    fincstp

    ; Point (1, 0)
    dec dword [RBP-16]
    getPoint dword [RBP-24], dword [RBP-16] ; R12 = (x + 1, y + 0)
    
    ; ST0 = Weight (1, 0)
    fld1                        ; ST0 = 1
    fsub ST0, ST1               ; ST0 = 1 - wY
    fmul ST0, ST2               ; ST0 = wX*(1 - wY) = w !!

    ; Add parts from this point to the resulting values
    addPart 0, [RBP-32]         ; B
    addPart 1, [RBP-36]         ; G
    addPart 2, [RBP-40]         ; R
    addPart 3, [RBP-44]         ; A

    ; Clear data from FPU stack
    ffree st0                   ; w
    ffree st1                   ; wY
    ffree st2                   ; wX
    fincstp
    fincstp
    fincstp

    ; Truncate the pixel values and save them
    cvttss2si R13D, dword [RBP-32]  ; B = (uchar)((int)tempResult:A)
    mov byte [R15], R13B        ; (*output:A) = A
    cvttss2si R13D, dword [RBP-36]  ; G
    mov byte [R15+1], R13B
    cvttss2si R13D, dword [RBP-40]  ; R
    mov byte [R15+2], R13B
    cvttss2si R13D, dword [RBP-44]  ; A
    mov byte [R15+3], R13B

    ; Decrement the x pointer
    dec R11
    jns loop

    ; Restore the previous x value
    mov R11, RSI
    dec R11

    ; Decrement the y pointer
    dec R10
    jns loop

end: 
    ; Clear the stack frame
    mov rsp, rbp
    pop rbp

    ; Restore R12 - R15 and RBX
    pop RBX
    pop R15
    pop R14
    pop R13
    pop R12

    ret

