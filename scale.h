/*
 * Bilinear scaling exports.
 * Implementation resides in scale.asm.
 * Bilinear scaling. Krotton, 2014.
 */

#pragma once

// Scale the given image data using bilinear interpolation.
// Input data is expected to be in RGB 8-8-8 format.
extern "C" void scale(uchar *output, int outW, int outH,
    const uchar *input, int inW, int inH);

