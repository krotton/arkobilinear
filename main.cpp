#include "mainwindow.h"
#include <QApplication>
#include <QMessageBox>

#include "avx.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    /*if (!avxSupported())
    {
        QMessageBox::critical(&w, "Error", "Your CPU doesn't support AVX!");
        return 1;
    }*/

    w.show();

    return a.exec();
}
