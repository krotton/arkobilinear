CONFIG += 32bit

QT += core gui

TEMPLATE = app

HEADERS += mainwindow.h
SOURCES += main.cpp mainwindow.cpp
FORMS += mainwindow.ui

CONFIG(64bit) {
    TARGET = bilinear
    ASM = avx.asm scale.asm
    NASM_CMD = nasm -f elf64 -gdwarf ${QMAKE_FILE_NAME}
}

CONFIG(32bit) {
    TARGET = bilinear32
    ASM = avx32.asm scale32.asm
    NASM_CMD = nasm -f elf32 -gdwarf -l ${QMAKE_FILE_BASE}.lst ${QMAKE_FILE_NAME}
}

CONFIG += debug qt

# Assemble with NASM
nasm.output = ${QMAKE_FILE_BASE}.o
nasm.commands = $$NASM_CMD
nasm.depends = $$ASM
nasm.input = ASM
nasm.variable_out = OBJECTS
QMAKE_EXTRA_COMPILERS += nasm

