#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QString>
#include <QFileDialog>
#include <QMessageBox>
#include <QPainter>

#include <sstream>

#include "scale.h"

void scale(uchar *output, int outW, int outH,
    const uchar *input, int inW, int inH);

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    rawData(NULL),
    displPic(NULL)
{
    ui->setupUi(this);

    fileSelectBtn = findChild<QPushButton*>("fileSelectBtn");
    sizeSlider = findChild<QSlider*>("sizeSlider");
    sizeResetBtn = findChild<QPushButton*>("sizeResetBtn");
    picFrame = findChild<QWidget*>("picFrame");
    statusBar = findChild<QStatusBar*>("statusBar");

    connect(fileSelectBtn, SIGNAL(clicked()),
        this, SLOT(selectFile()));
    connect(sizeSlider, SIGNAL(sliderMoved(int)),
        this, SLOT(resize(int)));
    connect(sizeResetBtn, SIGNAL(clicked()),
        this, SLOT(resetZoom()));
}

MainWindow::~MainWindow()
{
    if (displPic && displPic != &pic)
        delete displPic;
    if (rawData)
        delete rawData;

    delete ui;
}

void MainWindow::selectFile()
{
    QString fn = QFileDialog::getOpenFileName(this, "Select an image file",
        "", "Images (*.jpg *.bmp *.png *.xpm)");

    if (fn == "")
        return;

    QImage newPic(fn);

    // If loading failed, leave the current image untouched and notify the user.
    if (newPic.isNull())
    {
        QMessageBox::critical(this, "Error", "Couldn't load the selected picture!");
        return;
    }

    // Convert the image to ARGB32: AA-RR-GG-BB.
    newPic = newPic.convertToFormat(QImage::Format_ARGB32);

    if (newPic.isNull())
    {
        QMessageBox::critical(this, "Error", "Couldn't convert the picture to ARGB32");
        return;
    }

    pic = newPic;
    displPic = &pic;

    rescalePic();
    updateStatus();
}

void MainWindow::resize(int percent)
{
    if (displPic && displPic != &pic)
    {
        delete displPic;
        displPic = NULL;
    }

    if (rawData != NULL)
    {
        delete rawData;
        rawData = NULL;
    }

    int newW = (int)((percent/100.0) * pic.width()),
        newH = (int)((percent/100.0) * pic.height());

    if (newW < 2)
        newW = 2;
    if (newH < 2)
        newH = 2;

    rawData = new uchar[4 * newW * newH];

    // Execute the assembly!
    scale(rawData, newW, newH, pic.bits(), pic.width(), pic.height());

    displPic = new QImage(rawData, newW, newH, QImage::Format_ARGB32);

    update();
    updateStatus();
}

void MainWindow::moveFrame(int x, int y)
{
    if (!displPic)
        return;

    picBegin.setX(x);
    picBegin.setY(y);

    update();
}

void MainWindow::rescalePic()
{
    int sc = sizeSlider->value();

    if (sc == 100)
        displPic = &pic;
    else
        resize(sc);

    update();
}

void MainWindow::paintEvent(QPaintEvent *e)
{
    if (!displPic)
        return;

    QPainter p;
    p.begin(this);
    p.drawImage(picBegin, *displPic);
    p.end();
}

void MainWindow::resetZoom()
{
    sizeSlider->setValue(100);

    displPic = &pic;
    update();
    updateStatus();
}

void MainWindow::updateStatus()
{
    std::stringstream ss;
    ss << displPic->width() << "x" << displPic->height() << " (" <<
        sizeSlider->value() << "%)";

    statusBar->showMessage("Current size: " + QString(ss.str().c_str()));
}

// Showcast C implementation of scale()
const uchar *getPoint(const uchar *input, int x, int y, int imgW, int imgH)
{
    if (y >= imgH)
        y = imgH - 1;
    if (x >= imgW)
        x = imgW - 1;

    return input + 4 * y * imgW + 4 * x;
}

void _scale(uchar *output, int outW, int outH,
    const uchar *input, int inW, int inH)
{
    float xRatio = (float)inW/(float)outW, yRatio = (float)inH/(float)outH;

    for (int y = outH - 1; y >= 0; --y)
    {
        for (int x = outW - 1; x >= 0; --x)
        {
            float xSrc = xRatio * x, ySrc = yRatio * y;
            int xInt = int(xSrc), yInt = int(ySrc);
            float wX = xSrc - xInt, wY = ySrc - yInt;

            const uchar *p00 = getPoint(input, xInt, yInt, inW, inH),
                *p01 = getPoint(input, xInt, yInt + 1, inW, inH),
                *p11 = getPoint(input, xInt + 1, yInt + 1, inW, inH),
                *p10 = getPoint(input, xInt + 1, yInt, inW, inH);

            float p00w = (1 - wX) * (1 - wY),
                p01w = (1 - wX) * wY,
                p11w = wX * wY,
                p10w = wX * (1 - wY);

            int outPos = 4 * y * outW + 4 * x;

            for (int i = 0; i < 4; ++i)
                output[outPos + i] = (uchar)(p00w * p00[i] +
			        p01w * p01[i] +
			        p11w * p11[i] +
                    p10w * p10[i]);
        }
    }
}

