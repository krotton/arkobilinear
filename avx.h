/*
 * AVX support detection.
 * Implementation resides in avx.asm.
 * Bilinear scaling. Krotton, 2014.
 */

#pragma once

extern "C" bool avxSupported();

