; AVX support detection and helpers.

[global avxSupported]

avxSupported:
    mov EAX, 1
    cpuid

    xor EAX, EAX
    bt ECX, 28
    adc EAX, EAX

    ret

