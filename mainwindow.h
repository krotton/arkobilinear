#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QImage>
#include <QWidget>
#include <QPushButton>
#include <QSlider>
#include <QStatusBar>
#include <QPoint>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void selectFile();
    void moveFrame(int x, int y);
    void resize(int percent);
    void resetZoom();
    void paintEvent(QPaintEvent *e);

private:
    Ui::MainWindow *ui;
   
    uchar *rawData;
    QImage pic, *displPic;
    QPoint picBegin;

    QPushButton *fileSelectBtn;
    QSlider *sizeSlider;
    QPushButton *sizeResetBtn;
    QWidget *picFrame;
    QStatusBar *statusBar;

    void rescalePic();
    void updateStatus();
};

#endif // MAINWINDOW_H
